## [1.1.5] – 2023-10-24

### Fixed
- Cross-platform directories fixed (again)


## [1.1.4] – 2023-10-24

### Fixed
- Cross-platform directories fixed


## [1.1.3] – 2023-07-31

### Added
- Access to attributes list
- Version of assembly

### Fixed
- Fixed matching algorithm for attributes


## [1.1.2] – 2023-07-25

### Added
- Attributes for `ClassDeclarationInfo`, minor refactoring


## [1.1.1] – 2023-06-17

### Added
- `FieldDeclarationInfo` and `ClassDeclarationInfo` syntax wrappers


## [1.1.0] – 2023-06-16

### Added
- Base `ExtendedGenerator` class, that do looping through trees (files) and handles basic branching


## [1.0.0] – 2023-03-09

### Added
- `WebLoggedGenerator`, that logs source generation process to REST API
- Pack of extensions to `Microsoft.CodeAnalysis.CSharp.Syntax`, that helps unwrap C# syntax tree
